# hometask-10

## Ответьте на вопросы

#### 1. Как можно сменить контекст вызова? Перечислите все способы и их отличия друг от друга.
> Ответ: 1. Вызвать функцию в другом месте, соответственно и контекст поменяется; 2. метод bind, в него можно передать контекст и через запятую аргументы - это способ передать нужный контекст; 3. методы call и apply, при передачи контекста в них, фунция сразу вызывается. Отличаются они лишь тем, что аргументы у call  передаются через запятую, а у apply в виде массива.
#### 2. Что такое стрелочная функция?
> Ответ: Стрелочная функция - это альтернативная и более короткая запись функции, ее отличие от обычной заключается в том, что она не имеет своего контекста вызова.
#### 3. Приведите свой пример конструктора. 
```js
function Child(age, gender, colorEye, colorHair) {
  this.age = age;
  this.gender = gender;
  this.colorEye = colorEye;
  this.colorHair = colorHair;
}

const Sasha = new Child('5', 'girl', 'blue', 'blond');


```
#### 4. Исправьте код так, чтобы в `this` попадал нужный контекст. Исправить нужно 3мя способами, как показано в примерах урока.

```js
//1
  const person = {
    name: 'Nikita',
    sayHello: function() {
      setTimeout(function() {
          console.log(this.name + ' says hello to everyone!');
      }.bind(person), 1000)
    }
  }

  person.sayHello();
  //2
    const person = {
    name: 'Nikita',
    sayHello: function() {
      let thisVar = this;
      setTimeout(function() {
          console.log(thisVar.name + ' says hello to everyone!');
      }, 1000)
    }
  }

  person.sayHello();
//3
  const person = {
    name: 'Nikita',
    sayHello: function() {
      setTimeout(function() {
          console.log(this.name + ' says hello to everyone!');
      }, 1000)
    }
  }

  person.sayHello();
  
```

## Выполните задания

* Установите зависимости `npm install`;
* Допишите функции в `task.js`;
* Проверяйте себя при помощи тестов `npm run test`;
* Создайте Merge Request с решением.
